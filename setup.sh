#!/bin/bash

DOMAIN=${1:-192.168.7.21}
VERSION=${2:-2.3.4}

bin/download $VERSION

echo "Your system password has been requested to add an entry to /etc/hosts..."
echo "127.0.0.1 ::1 $DOMAIN" | sudo tee -a /etc/hosts

bin/setup $DOMAIN
bin/fixowns