#!/bin/bash

cd php7/
docker build -t qponline/php7:fpm . -f Dockerfile

cd ../nginx/
docker build -t qponline/nginx:latest . -f Dockerfile

cd ../elasticsearch/7.6/
docker build -t qponline/elasticsearch:7.6 . -f Dockerfile

cd ..
